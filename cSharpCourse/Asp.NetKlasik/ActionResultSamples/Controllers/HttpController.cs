﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class HttpController : Controller
    {
        // GET: Http
        public HttpStatusCodeResult Index()
        {
           // return new HttpStatusCodeResult(HttpStatusCode.BadGateway); // http view de hata sayfası çıkartmak için kullanılabilir
          // return new HttpStatusCodeResult(HttpStatusCode.BadGateway, "Bad Gateway From MVC"); // varsayılan http durum text i yerine bu şekilde istediğimiz hata yazısını ekleyebiliyoruz.
          return new HttpStatusCodeResult(502); // varsayılan hata kodu olan 502 hata kodunu döndürür. ilk return ifadesi ile aynıdır.


        }

        public HttpUnauthorizedResult Index2()
        {
            return new HttpUnauthorizedResult(); // 401 nolu hata kodunu döndürür
        }
        // HttpUnauthorizedResult ve HttpNotFoundResult hata durumlarında HttpStatusCodeResult ta olduğu gibi varsayılan hata mesajı düzenlenebilir
        public HttpNotFoundResult Index3()
        {
            return new HttpNotFoundResult();  // 404 nolu hata kodunu döndürür 
        }
    }
}