function s(isim:string) {
    return isim
}

let cıktı = s("Sümeyye")

console.log(cıktı)



let ad:string = "Ali";
let sayi:number = 12;
let sayilar:number[] = [1,23,45] // number array
let sayilar2: Array<number> = [2,3,4] // number array

let dizi:[number,string] = [12, "merhba"] // hangi türde değişken atamsı yapılacaksa çoklu şekilde belirtilebilir


enum Renk {Kirmizi, Sari, Pembe}
let renk : Renk.Kirmizi // enum kullnımı

let deneme:any = "Deneme"; // any veri türü sabit bir tip ataması yapmadan number, bool, sring gibi veri türlerini ihtiyaca göre sorunsuz kullılmAINI sağlar.
deneme = 12;
deneme = true;   


function topla(x,y) { // bu fonksiyonda js'de olduğu gibi herhangi bir tip güvenliği olmadığı için gönderilen number ve string veri türünü direkt olarak yan yana yazdırı.
    // ts tip güvenliği ile bu durumun önüne geçer.
    return x + y
}
console.log(topla(10,"Samsun"))

function topla1(number1:number, number2:number):number{
    return number1 + number2;
}
console.log(topla1(1,2));

function topla2(sayi1:number, sayi2:number):void{ // fonksiyonun dönüş değeri void olduğu için fonksiyon içerde bir return alamaz.
    console.log(sayi1+sayi2);
}
topla2(2,3);



function topla3(number1:number, number2:number=3):number { // burda varsayılan değer olarak bir değer atadık. js de olduğu gibi varsayılan bir değer atamama durumunda undif dönmez program hata verir.
    return number1 + number2;
}
console.log(topla3(20));

function topla4(number1:number, number2?:number):number { // number2 değişkeninin yanına ? eklenmiş olması, değer atanmamsı durumunda asıl bir işlem gerçekleştirmesi gerektiğini gösterir.
    if(number2){
        return number1 + number2
    }
    return number1
}
console.log(topla4(90))