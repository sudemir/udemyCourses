﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace AdoNetDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ProductDal _productDal= new ProductDal();
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadProducts();
        }

        public void LoadProducts()
        {
            dgwProducts.DataSource = _productDal.GetAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _productDal.Add(new Product
            {
                Name = textBox1.Text,
                UnitPrice = Convert.ToDecimal(textBox2.Text),
                StockAmount = Convert.ToInt32(textBox3.Text)
            }) ;
            MessageBox.Show("Product Added");
            LoadProducts();

        }

        private void dgwProducts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("Hücre Seçildi");

            textBox4.Text = dgwProducts.CurrentRow.Cells[1].Value.ToString();
            textBox5.Text = dgwProducts.CurrentRow.Cells[2].Value.ToString();
            textBox6.Text = dgwProducts.CurrentRow.Cells[3].Value.ToString();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Product product = new Product
            {
                Id = Convert.ToInt32(dgwProducts.CurrentRow.Cells[0].Value),
                Name = textBox4.Text,
                UnitPrice = Convert.ToDecimal(textBox5.Text),
                StockAmount= Convert.ToInt32(textBox6.Text)

        };
            _productDal.Update(product);
            LoadProducts();
            MessageBox.Show("Hücre Güncellendi ");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(dgwProducts.CurrentRow.Cells[0].Value);
            _productDal.Delete(id);
            LoadProducts();
            MessageBox.Show("Hücre verisi silindi");
        }
    }
}
