﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class FileController : Controller
    {
        // GET: File
        public FileResult Index()
        {
            //return File("~/Content/goliq.doc", "application/msword"); // burda birinci parametre olarak dosya yolunu ikinci parametre olarak conetType'nı veriyoruz.
            //return File("~/Content/goliq.doc", "application/msword", "goliq.doc"); // burda ise download edilecek dosyanın ismini üçüncü parametre olarak verebiliyoruz
             return File("~/Content/goliq.doc", "text/plain", "goliq.txt"); // doc dosyası için yaptığımız işlemi txt dosyası için de yapabiliyoruz.
        }
    }
}