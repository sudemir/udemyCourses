﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CokluImplementasyon
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IWorker[] worker = new IWorker[3]
            {
                new Manager(),
                new Worker(),
                new Robot()
            };
            ISalary[] salary = new ISalary[]
            {
                new Manager(),
                new Worker(),
                // new Robot() => Robot classına Salary interfacesi implemente edilmediği için bu new Robot ifadesini burda kullanamıyoruz.
            };

            foreach (var workers in worker)
            {
                workers.Work();

            }

            foreach (var salarys in salary)
            {
                salarys.GetSalary();
            }

            Console.ReadLine();
        }

        interface IWorker
        {
            void Work();

        }
        interface IEat
        {
            void Eat();
        }
        interface ISalary
        {
            void GetSalary();

        }
        class Manager : IWorker, IEat, ISalary
        {
            public void Work()
            {
                Console.WriteLine("Müdür çalışıyor");

            }
            public void Eat()
            {
                Console.WriteLine("Müdür yemek yiyor");

            }
            public void GetSalary()
            {
                Console.WriteLine("Müdür maaş alıyor");

            }

        }
        class Worker : IWorker, IEat, ISalary
        {
            public void Work()
            {
                Console.WriteLine("Çalışan");
            }
            public void Eat()
            {
                Console.WriteLine("Çalışan yamey yiyor");

            }
            public void GetSalary()
            {
                Console.WriteLine("Çalışan maaş alıyor");
            }

        }
        class Robot : IWorker
        {
            public void Work()
            {
                Console.WriteLine("Robot Çalışıyor");
            }
        }
    }
}
