function s(isim) {
    return isim;
}
var cıktı = s("Sümeyye");
console.log(cıktı);
var ad = "Ali";
var sayi = 12;
var sayilar = [1, 23, 45]; // number array
var sayilar2 = [2, 3, 4]; // number array
var dizi = [12, "merhba"]; // hangi türde değişken atamsı yapılacaksa çoklu şekilde belirtilebilir
var Renk;
(function (Renk) {
    Renk[Renk["Kirmizi"] = 0] = "Kirmizi";
    Renk[Renk["Sari"] = 1] = "Sari";
    Renk[Renk["Pembe"] = 2] = "Pembe";
})(Renk || (Renk = {}));
var renk; // enum kullnımı
var deneme = "Deneme"; // any veri türü sabit bir tip ataması yapmadan number, bool, sring gibi veri türlerini ihtiyaca göre sorunsuz kullılmAINI sağlar.
deneme = 12;
deneme = true;
function topla(x, y) {
    // ts tip güvenliği ile bu durumun önüne geçer.
    return x + y;
}
console.log(topla(10, "Samsun"));
function topla1(number1, number2) {
    return number1 + number2;
}
console.log(topla1(1, 2));
function topla2(sayi1, sayi2) {
    console.log(sayi1 + sayi2);
}
topla2(2, 3);
function topla3(number1, number2) {
    if (number2 === void 0) { number2 = 3; }
    return number1 + number2;
}
console.log(topla3(20));
function topla4(number1, number2) {
    if (number2) {
        return number1 + number2;
    }
    return number1;
}
console.log(topla4(90));
