﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class JavaScriptController : Controller
    {
        // GET: JavaScript
        public ActionResult Index()
        {
            return View();
        }
        public JavaScriptResult Alert()
        {
            return JavaScript("alert('Hello World');");
        }
    }
}