﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelBinding.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var controler = RouteData.Values["controller"]; // RouteDataSınıfı, istenen bir yolun değerlerini içerir. Genellikle, RouteData yöntemini çağırarak bir nesnesi alırsınız 
            var action = RouteData.Values["action"];
            var id = RouteData.Values["id"];



            var lastName = Request.QueryString["lastName"]; // QueryString yapısı ile internet tarayıcımızın URL satırını kullanarak mevcut linklere eklemeler yaparak göndermek istediğimiz veriyi başka bir sayfaya gönderebiliriz.
            var productId = Request.QueryString["productId"]; // /?lastName=demir&productId=12 şeklinde url ekleme yapılabilir
                                                              // return Content(string.Format("Controller{0} - Action{1}", controler,action));

            return RedirectToAction("Index2", new { controler = "Home", id = 12, fullName = "Engin demiroğ" }); // fullName adında bir route tanımlanmadığı için bunu queryString olarak url e ekler
        }
        public ActionResult Index2()
        {
            return new EmptyResult();
        }
    }
}