﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace EntityOrnek
{
    public partial class Form1 : Form
    {
        DbSinavOgrenciEntities ogrdb = new DbSinavOgrenciEntities();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
        private void Clear()
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
            textBox4.Clear();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            SqlConnection baglan = new SqlConnection(@"Data Source=DESKTOP-VEB7F92;Initial Catalog=DbSinavOgrenci;Integrated Security=True");
            SqlCommand komut = new SqlCommand("select * from TBLDERSLER", baglan);
            SqlDataAdapter da = new SqlDataAdapter(komut);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource= dt;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = ogrdb.TBLOGRENCİ.ToList(); // veritabanından verileri çekme işlemi entity ile bu komutlar sayesinde yapılabiliyor

            dataGridView1.Columns[3].Visible = false; // bu kullanım sayesinde tabloda görüntülemek istemediğimiz kolonları gizliyebiliyoruz. 
            dataGridView1.Columns[4].Visible = false;

        }

        private void button9_Click(object sender, EventArgs e)
        {
            var query = from item in ogrdb.TBLNOTLARs // TBLOGRENCİ tablosunda olduğu gibi listelemek istediğimiz alanları ayrıca bu şekilde de yazarak listeleybiliriz.
                        select new
                        { item.NOTID, item.OGR, item.DERS, item.SINAV1, item.SINAV2, item.SINAV3 };
            dataGridView1.DataSource = query.ToList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TBLOGRENCİ ogrenciKaydet = new TBLOGRENCİ();
            ogrenciKaydet.AD = textBox2.Text;
            ogrenciKaydet.SOYAD= textBox3.Text;
            ogrdb.TBLOGRENCİ.Add(ogrenciKaydet);
            ogrdb.SaveChanges(); // en son yapılan güncellemelri veri tabanına kaydeder.
            MessageBox.Show("KAydetme işlemi başarılı!!!");


        }

        private void button10_Click(object sender, EventArgs e)
        {
            TBLDERSLER dersKaydet = new TBLDERSLER();
            dersKaydet.DERSAD = textBox11.Text;
            ogrdb.TBLDERSLERs.Add(dersKaydet);
            ogrdb.SaveChanges();
            MessageBox.Show("Ders kaydetme işlemi başarılı");

        }

        private void button3_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBox1.Text); // entitiy framework ile silme işlemi
            var x = ogrdb.TBLOGRENCİ.Find(id);
            ogrdb.TBLOGRENCİ.Remove(x);
            MessageBox.Show("Sileme işlemi başarılı");
            ogrdb.SaveChanges();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(textBox1.Text); // entitiy framework ile güncellem işlemi
            var x = ogrdb.TBLOGRENCİ.Find(id);
            x.AD = textBox2.Text;
            x.SOYAD = textBox3.Text;
            x.FOTO = textBox4.Text;
            ogrdb.SaveChanges();
            Clear();
            MessageBox.Show("Öğrenci güncellme işlemi başarılı");
        }
    }
}
