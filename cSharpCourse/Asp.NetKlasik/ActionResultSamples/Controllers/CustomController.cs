﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class CustomController : Controller
    {
        // GET: Custom
        public class CustomResult : ActionResult
        {
            public override void ExecuteResult(ControllerContext context)
            {
                context.HttpContext.Response.Write("My Customer ResultAction"); // burda ActionResult nesnesinden türeyen CustomResult classsını oluşturduk ve bunu kullanık.
            }
        }

        public ActionResult Index()
        {
            return new CustomResult();
        }

        // ActionMethodSelectors
        [NonAction] // NoAction: action olarak kullanılmasını istemdiğimiz metodlara ekleyerek action olarak kullılmasını engellemek için eklenir. Böylelikle bu metod çağırılsa bile 404 hata kodu döner
        public string GetConnectionString()
        {
            return "my connection string";
        }

        [ActionName("Response")] // ActionName: aşağıdaki aksiyonun test ismi ile değil de actionName özelliğinde belirtilen aksiyon ismi ile çağırılmasını sağlar. Değer döner fakat CustomViewdeki test.cshtml dosyası isminin de değiştirilmesi halinde sayfa görüntülenebilir.
        public ActionResult Test()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get)] // AcceptVerbs : gelen http isteğinin türüne göre aksiyon üzerinde filitrelemeler yapabilmemeizi sağlar. Buradaki kullnım aksiyonumuzun sadece get türündeki isteklere cevap vermesi için kullanılabilir.
        [HttpPost] // yukarıdaki kullanım ile aynıdır. Bu da sadece post türünde isteklere cevap verilmesini sağlar.
        [AcceptVerbs (HttpVerbs.Post |HttpVerbs.Get)] // bu kullanım ise hem get hem de post isteklerinin success olarak dönmesini sağlar.
        [AcceptVerbs("Put", "Get", "Post")] // bu kullanım ile de istekleri parametre olarak da verebilirz.
    
    }
}