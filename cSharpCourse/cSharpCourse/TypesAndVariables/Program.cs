﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TypesAndVariables
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("hello world");
            int number1 = 2147483647;
            long number2 = 9223372036854775807;
            short number3 = 32767;
            byte number4 = 255;
            bool condition = false;
            char caracter = 'A';
            char caracter1 = 'A';
            double number5 = 12.4;

            Console.WriteLine(number1);
            Console.WriteLine(number2);
            Console.WriteLine(number3);
            Console.WriteLine(number4);
            Console.WriteLine(condition);
            Console.WriteLine((int)caracter); // char veri tipleri sayıların, ascıı tablosnudaki string karşılığını verir. Bu karakterin ascıı tablosundaki sayısal karşılığını görmek için bu şekide bir kullanımda bulunabiliriz.
            Console.WriteLine(caracter1);
            Console.WriteLine(number5);
            Console.ReadLine();
        }
    }
}
