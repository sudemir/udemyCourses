var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Kişi = /** @class */ (function () {
    function Kişi() {
    }
    Kişi.prototype.kaydet = function () {
        console.log("Müşteri kaydedildi");
    };
    return Kişi;
}());
var Müşteri = /** @class */ (function (_super) {
    __extends(Müşteri, _super);
    function Müşteri() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Müşteri.prototype.satişYap = function () {
        console.log("Müşteriye satış yapıldı");
    };
    return Müşteri;
}(Kişi));
var Personel = /** @class */ (function (_super) {
    __extends(Personel, _super);
    function Personel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Personel.prototype.maaşÖde = function () {
        console.log("Maaş ödendi");
    };
    return Personel;
}(Kişi));
var müsteri = new Müşteri();
müsteri.kaydet();
müsteri.satişYap();
var personel = new Personel();
personel.maaşÖde();
