﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    internal class Program
    {
        static void Main(string[] args)
        {
            PersonManager manager = new PersonManager();
            Customer customer = new Customer {
                firstName = "Sümeyye",
                lastName = "Demir",
                phoneNumber = 552365897,
                address = "Şanlıurfa"
            };
            Student student = new Student
            {
                firstName = "Ahmet",
                lastName = "NamNam",
                phoneNumber = 553698121,
                departman = "Bilişim"
            };
            manager.Add(customer);
            manager.Add(student);


            CustomerMAnager customerMAnager = new CustomerMAnager();
            customerMAnager.Add(new OreacleDatabase());
            Console.WriteLine("******************");


            // bu kuıllanım ile Interfacesi alınan nesneleri foreach ile dönerek tek bir defada yazdırmamızı sağlar
            ICustomerDal[] customerDals = new ICustomerDal[]
            {
                new SqlServerCustomerDal(),
                new OreacleDatabase()
             };
            foreach (var customerDal in customerDals)
            {
                customerDal.Add();
            }





            Console.ReadLine();

        }




    }


    // Interfaces : yapısı gereği diğer sınıflara yol gösterici,
    // rehberlik yapmak için oluşturulan, kendisinden implement edilenbir sınıfa doldurulması zorunlu olan
    // bazı özelliklerin aktarılmasını sağlayan kavramdır.
    interface IPerson
    {
        string firstName { get; set; }
        string lastName { get; set; }

        int phoneNumber { get; set; }
    }
    class Customer:IPerson
    {
        public string firstName { get; set;}
        public string lastName { get; set;}
        public int phoneNumber { get; set;}
        
        public string address { get; set;} // Diğer üç alan IPerson Interfacesine ait alandan gelirken bu alan Customer class yapısına ait bir alandır

    }
    class Student:IPerson
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int phoneNumber { get; set; }

        public string departman { get; set; } //  Diğer üç alan IPerson Interfacesine ait alandan gelirken bu alan Student class yapısına ait bir alandır
    }
    class PersonManager
    {
        public void Add(IPerson person)
        {
            Console.WriteLine(person.firstName);
        }

    }
}
