var Ev = /** @class */ (function () {
    function Ev(odaSayisi, katSayisi, pencereSayisi) {
        this._katSayisi = katSayisi;
        this._odaSayisi = odaSayisi;
        this._pencereSayisi = pencereSayisi;
    }
    Ev.prototype.yemekya = function () {
        console.log(this._katSayisi + ". kat yemek yedi");
    };
    return Ev;
}());
var ev = new Ev(2, 3, 4);
ev.yemekya();
