﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VirtulaMethods
{

        // C# Virtual Method Nedir?
        // Kalıtım yoluyla sınıfların türetilebileceğini ve bir sınıfın diğer bir sınıftan türediği zaman, türediği sınıfın bütün özelliklerini içereceğini C# Inheritance (Kalıtım) başlıklı içeriğimizde açıklamıştık.
        //Temel sınıftan türetilmiş sınıflara aktarılan metotları her zaman olduğu gibi kullanmak istemeyebiliriz.Bu metotları türetilmiş sınıfın içerisinden yeniden tanımlayabilmek için virtual ve override anahtar sözcüklerini kullanırız.
        //Virtual metotlar Inheritance(Kalıtım) yoluyla aktarıldıkları sınıfların içerisinden override edilerek değiştirilebilirler.Eğer override edilmezlerse temel sınıf içerisinde tanımladıkları şekilde çalışırlar.
    internal class Program
    {
        static void Main(string[] args)
        {
            SqlServer sqlserver= new SqlServer();
            sqlserver.Add();

            MySql mysql= new MySql();
            mysql.Add();

            Console.ReadLine();
        }
        class Database
        {
            public virtual void Add()    { Console.WriteLine("Added by default"); }
            public void Update() { Console.WriteLine("Update by default"); }
        }
        class SqlServer : Database
        {
            public override void Add()
            {
                Console.WriteLine("Sql sever data elendi !!");
                //base.Add(); // base.Add(), kullanıldığı taktirde mirası aynı şekilde alır ve kullnır.
            }


        }
        class MySql : Database
        {

        }
    }
}
