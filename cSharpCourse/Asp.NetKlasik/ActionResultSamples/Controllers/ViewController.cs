﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class ViewController : Controller
    {
        // GET: View
        public ActionResult Index() // Tüm result çeşitleri ActionResult'tan türer
        {
            return View();
        }

        public ActionResult Index2()
        {
            return View();
        }

        public ActionResult Index3()
        {
            return View("Index"); // bnu kullanım ile ındex3 türünde bir sayfa aramnası halınde ındex adlı View'e gider ve onun içindeki çıktıyı verir
        }

        public PartialViewResult Partial() // View Resul da geçerli işlemleri aynı şekilde PartialViewResult actionunda kullanabiliyoruz
        {
            return PartialView();
        }
    }
}