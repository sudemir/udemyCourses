﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class ContentController : Controller
    {
        // GET: Content
        public ContentResult Index()
        {
            return Content("SümeyyeDemir");
        }
        public ContentResult Index2()
        {
            return Content("<b>Salih Demiroğ<b>");
        }
    }
}