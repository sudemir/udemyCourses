﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections_ArrayList
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ArrayList dinamikdizi = new ArrayList(); // Arraylistler tip güvenlidir (string,int,bool farketmeksizin verilebilir) ve diziye devamlı ekleme yapılacak durumlarda kullanır. 
            dinamikdizi.Add("Ankara");               // bellek üzerinde devamlı alan alıp dizi boyutunu aşacak durumda eklemeler yapıldığında sorun yaşanmaması için kullanımı daha iyidir.
            dinamikdizi.Add("İstanbul");
            foreach (var city in dinamikdizi)
            {
                Console.WriteLine(city);
            }

            dinamikdizi.Add("Şanlıurfa");
            Console.WriteLine(dinamikdizi[2]); // bu örnekteki gibi diziye istendiği zamanda ekleme yapılabilir. 





            List<string> cities = new List<string>(); // bu kullanım da tip güveli collections lara öenektir
            cities.Add("Samsun");
            cities.Add("Eskişehir");
            cities.Add("Bursa");
            foreach (var city in cities)
            {
                Console.WriteLine(city);

            }




            List<Customer> customer = new List<Customer>();
            customer.Add(new Customer { ıd = 1, FirstName = "Engin" });
            customer.Add(new Customer { ıd=2, FirstName= "Ali"});
            foreach (var customers in customer)
            {
                Console.WriteLine(customers.FirstName);
            }

            Console.ReadLine();
        }
    }

    class Customer
    {
        public int ıd { get; set; }
        public string FirstName { get; set; }
    }
}
