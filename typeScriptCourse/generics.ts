function deger(number:number):number {
    return number;
}
console.log(deger(12))

function deger2(sehir:string): string{
    return sehir;
}
console.log(deger2("Sümeyye"));

// Generics : değişkene hangi tipte değer atayacaksak o türde değer atamaya yardımcı olur

function deger3<T>(deger:T):T{
    return deger
}
let sayi = deger3<number>(90);
let deneme = deger3<string>("Ankara")
console.log(sayi)
console.log(deneme)


// Generics Class
class GenericsClass<T>{
    degisken:T
    kaydet(deneme:T):T {
        return deneme
    }
}

 let parametre = new GenericsClass();

 let nam1 = parametre.degisken = "ahmet";
 let nam2 = parametre.kaydet(12)
 console.log(nam1);
 console.log(nam2)