abstract class KrediBase{
    constructor(){
    }

    kaydet():void{
        console.log("Kullanıcı kaydedildi")
    }
    abstract hesapla():void;
}
class TükediciKredi extends KrediBase{
    constructor(){
        super();
    }
    hesapla():void{
        console.log("Tüketici kredisine göre hesap yapıldı")
    }
}

class MortgageKredi extends KrediBase{
    constructor(){
        super();
    }
    hesapla():void{
        console.log("Konut kredisine göre hesap yapıldı")
    }
}

// let tuketicikredi = new TükediciKredi();
// tuketicikredi.hesapla();
// tuketicikredi.kaydet();

// let mortgagekredi = new MortgageKredi();
// mortgagekredi.hesapla();
// mortgagekredi.kaydet();



let kredi: KrediBase
kredi = new TükediciKredi
kredi = new MortgageKredi

kredi.hesapla();