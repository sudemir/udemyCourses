﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{

    public class Product
    { 
        public string Name { get; set; }
        public int UnitPrice { get; set; }
    }
    public class JsonController : Controller
    {
        // GET: Json
        public JsonResult Index() // JsonResult : json türünde değer döndürmemizi sağlar
        {
            var products = new List<Product>
            {
                new Product
                {
                    Name = "Laptop",
                    UnitPrice = 120
                },
                new Product
                {
                    Name = "Mouse",
                    UnitPrice  = 150
                }
            };

            return Json(products, "application/json", Encoding.UTF8, JsonRequestBehavior.AllowGet);
        }
    }
}