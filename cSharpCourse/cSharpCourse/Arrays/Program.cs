﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays
{
    internal class Program
    {
        static void Main(string[] args)
        {

            // arraylerin iki türlü kullanımı vardır for ve foreach döngüleri ile kullanımları örnekteki gibidir.

            //string[] ogr = new string[3] { "Recep", "şaban", "ramazan" };
            //foreach (var ogrenciler in ogr)
            //{
            //    Console.WriteLine(ogrenciler);
            //}

            //string[] students = { "sümeyye", "cemile", "ayşenur", "büşra" };
            //for (int i = 0; i <=students.Length; i++)
            //{
            //    Console.WriteLine(students[i]);

            //}



            // çok boyutlu diziler
            string[,] regions = new string[3, 3] {
                {"İstanbul ", "Tekirdağ ", "Edirne " },
                {"antep ", "şanlıurfa ", "mardin " },
                {"trabzon ", "rize ", "ordu " }
            };
            for (int i = 0; i < regions.GetLength(0); i++)
            {
                for (int j = 0; j < regions.GetLength(1); j++)
                {
                    Console.Write(regions[i, j]);
                }
                Console.WriteLine();
            }



            // çok boyutlu diziler
            int[,] regions1 = new int[3, 3] {
               {1,2,3},
               {4,5,6},
               {7,8,9}
            };
            for (int i = 0; i < regions.GetLength(0); i++)
            {
                for (int j = 0; j < regions.GetLength(1); j++)
                {
                    Console.Write(regions1[i, j] + "\t");
                }
                Console.WriteLine();
            }
            Console.ReadLine();



        }
    }
}
