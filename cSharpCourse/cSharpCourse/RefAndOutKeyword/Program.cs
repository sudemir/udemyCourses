﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RefAndOutKeyword
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Add(2, 4, 6, 8, 10));
            Console.ReadLine();

        }

        // params : C#'ta params, değişken sayıda argüman alan bir parametreyi belirtmek için kullanılan bir anahtar kelimedir. Önceden argüman sayısını bilmediğimizde kullanışlıdır.
        // paramslar metodlara aynı türde istediğimiz kadar parametre gönderebileceğimiz yapılardır.

        static int Add(params int[] numbers)
        {
            return numbers.Sum();
        }
    }
}
