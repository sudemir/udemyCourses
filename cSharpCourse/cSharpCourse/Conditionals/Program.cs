﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Conditionals
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Add();
            var result = Topla(23);
            Console.WriteLine(result);

            int sayi1 = 10;
            int sayi2 = 20;
            var sonuc2 = Add2(sayi1,sayi2);
            Console.WriteLine(sayi1);
            Console.WriteLine(sonuc2);
            Console.ReadLine();
        }
        static void Add()
        //void: Bu ifade değer döndermeden yapılacak fonksiyonel işlemlerin gerçekleştirilmesi için kullanılır.
        //Diğer tarafa sonucu göndermez sonuc ya o class içerisinde kalır ya da ekrana yazdırılabilir.
        {
            Console.WriteLine("Added!!!!");
        }
        static int Topla(int num1=50, int num2=10) {
            var sonuc =  num1 + num2;
            return sonuc;
        }

        static int Add2(int s1, int s2)
        {
            s1 = 30;
            var sonuc2 = s1 + s2;
            return sonuc2;
        }
    }
}
