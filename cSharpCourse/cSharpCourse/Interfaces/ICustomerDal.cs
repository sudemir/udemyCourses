﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
     interface ICustomerDal
    {
        void Add();
        void Update();
        void Delete();

    }
    class SqlServerCustomerDal:ICustomerDal
    {
        public void Add() {
            Console.WriteLine("SqlServerAdded!!");
        }

        public void Update() 
        { 
            Console.WriteLine("SqlServerUpdated!!");
        }
        public void Delete()
        {
            Console.WriteLine("SqlServerDeleted");
        }
    }
    class OreacleDatabase:ICustomerDal
    {
        public void Add()
        {
            Console.WriteLine("Oracle Added!");
        }
        public void Update()
        {
            Console.WriteLine("Oracle Updated");
        }
        public void Delete()
        {
            Console.WriteLine("Oracle Deleted");
        }
    }
    class CustomerMAnager
    {
        public void Add(ICustomerDal customerDal)
        {
            customerDal.Add();
        }
    }
}
