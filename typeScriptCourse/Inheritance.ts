class Kişi{
    
    kaydet(){
        console.log("Müşteri kaydedildi")
    }
}
class Müşteri extends Kişi{
    satişYap(){
        console.log("Müşteriye satış yapıldı")
    }
}
class Personel  extends Kişi{
    maaşÖde(){
        console.log("Maaş ödendi")
    }
}

let müsteri = new Müşteri();
müsteri.kaydet();
müsteri.satişYap();

let personel = new Personel();
personel.maaşÖde();