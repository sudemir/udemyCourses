let sehir = ["Ankara", "Şanlıurfa", "Mardin"]

for(let i in sehir){ // for in döngüsü dizinin indexini döner
    console.log(i)
}

for(let i of sehir){ // for of dizinin index değerini döner
    console.log(i)
}