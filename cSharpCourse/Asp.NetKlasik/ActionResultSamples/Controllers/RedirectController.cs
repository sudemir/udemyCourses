﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class RedirectController : Controller
    {
        // GET: Redirect
        public ActionResult Index()
        {
            return View();
        }
        public RedirectResult Index3() // RedirectResult : Uygulama içerisinde url üzerinden yönlendirme yapar
        {
            // return Redirect("~view");
            return Redirect("http://microsoft.com");
        }

        public RedirectToRouteResult Index4()
        {
            return RedirectToAction("Index", new { controller = "View", id = 5 }); //RedirectToRouteResult ile url'e controller ve id parametrelerini vererek ilgili yönlendirmeyi yapabiliyoruz.
        }
    }
}