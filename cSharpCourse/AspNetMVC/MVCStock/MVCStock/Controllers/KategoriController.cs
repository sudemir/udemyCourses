﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCStock.Models.Entity;

namespace MVCStock.Controllers
{
    public class KategoriController : Controller
    {
        // GET: Kategori

        MvcDbStockEntities db = new MvcDbStockEntities();
        public ActionResult Index()
        {
            var degerler = db.TBLKATEGORILER.ToList();
            return View(degerler);
        }

        [HttpGet]
        public ActionResult YeniKategori()
        {
            return View();
        }

        [HttpPost] // httpPost un kullanılmsı yeni kategori ekleme işleminde yanlızca herhangi bir değer döndürme durumunda ekleme işlemi yapar. aski durumda boş değer göndermez. Boş değer gönderme durumuda httpGet metodu çalışacak ve sayfaya null bir veri gönderilmeyecek
        public ActionResult YeniKategori(TBLKATEGORILER p1)
        {
            if (!ModelState.IsValid)
            {
                return View("YeniKategori");
            }
            db.TBLKATEGORILER.Add(p1);
            db.SaveChanges();
            return View();
        }
        public ActionResult Sil(int id)
        {
            var ketgori = db.TBLKATEGORILER.Find(id);
            db.TBLKATEGORILER.Remove(ketgori);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Guncelle(int id)
        {
            var ktgr = db.TBLKATEGORILER.Find(id);
            return View(ktgr);
        }
        public ActionResult KategoriGuncelle(TBLKATEGORILER p1)
        {
            var ktgrGuncelle = db.TBLKATEGORILER.Find(p1.KategoryId);
            ktgrGuncelle.KategoryAd = p1.KategoryAd;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}