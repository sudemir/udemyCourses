﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringMethods
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string sentence = "My name is sümeyye";
            var result = sentence.Length; // Değişkene atanmış stringin karakter uzunluğunu verir.
            var result2 = sentence.Clone(); // result2 değişkenine sentence adlı değişkene atanmış string clonlanır ve result2 artık bu stringi taşır.
            var result3 = sentence.EndsWith("e"); // metin stringinin en sonunda aranan kelime var mı yokmu bakar.Eğer varsa “True” yoksa “False” döner.
            var result4 = sentence.StartsWith("My"); //  metin stringi aranan kelimelerlemi başlıyor bakar.True ya da False döner.
            var result5 = sentence.IndexOf("e"); // yazılan stringin kaçıncı indexte olduğu bilgisini verir.
            var result6 = sentence.LastIndexOf("e"); // IndexOf gibi içerisine yazılan stringin kaçıncıcı indexe karşılık geldiğine bakar fakat aramaya en sondan başlar ve ona göre bir çıktı verir.
            var result7 = sentence.Insert(0, "Hello, "); // Bu metod sayesinde istediğimiz indexten sonrasına istediğimiz kelimeyi ekletebiliriz.
            var result8 = sentence.Substring(2); // Substring metodu parametre içerisinde verilen indexten itibaren metni almamızı sağlar.
            var result9 = sentence.Substring(3,4); // bu kullanım da 3. karakterden itibaren sadece dört karakter alabileceğimizi gösterir.
            var result10 = sentence.ToLower(); // metnin tamamını küçük karakterlerle yazmaya yarar.
            var result11 = sentence.ToUpper(); // metnin tamamını büyük karakterlerle yazmaya yarar.
            var result12 = sentence.Replace(" ", "-"); // Replace(‘bu değerleri’,’buna cevir’); = Bu metod iki parametre almaktadır.Birincisine değiştirilmek istenen karakter,ikincisine yerine getirilecek karakterdir.
            var result13 = sentence.Remove(2); // remove metodu içerisine verilen index sayısından sonraki ifadelerin ekrana çıktısını vermez.

            Console.WriteLine(result13);
            Console.ReadLine();
        }
    }
}
