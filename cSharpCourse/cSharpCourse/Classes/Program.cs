﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CostumerManager costumerManager = new CostumerManager();
            costumerManager.Add();
            costumerManager.Remove();

            ProductManager productManager = new ProductManager();
            productManager.Add();
            productManager.Update();





            // Classlar içinde property tanımlamak
            Customer customer = new Customer();
            customer.Id = 1;
            customer.Name = "Sümeyye";
            customer.Address = "Altındağ";
            customer.City = "Ankara";

            Console.WriteLine(customer.Name);

            Customer customer1 = new Customer
            {
                Id = 2,
                Name = "Ahmet",
                Address = "Karaköprü",
                City = "Şanlıurfa"
            };

            Console.WriteLine(customer1.Name);
            Console.ReadLine();
        }
    }
}
