﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructors
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CustomerManager customerManager = new CustomerManager(10);
            customerManager.Add();


            Teacher.Number = 10; // static nesneler new lenmez
            Console.ReadLine();
        }

        class CustomerManager
        {
            // Yapılandırıcıların (constructor) görevi oluşturulan nesneyi ilk kullanıma hazırlamasıdır. C# da tüm sınıflar (class) tanımlansın ya da tanımlanmasın değer tiplerine sıfır, referans tiplerine "null" değerini atayan varsayılan bir yapılandırıcı vardır. Yapılandırıcısı tanımlandıktan sonra varsayılan yapılandırıcı bir daha kullanılmaz. Yapılandırıcıların bilinen temel özellikleri:
            // Kendi sınıfı ile aynı isme sahip olması,
            // Genellikle açık bir dönüş tipi olmaması,
            // Başka sınıflar tarafından kullanılabilmesi için erişimin public (aleni) olmasıdır.
            private int _number;
            public CustomerManager(int number)
            {
                _number = number;
            }
            public void Add()
            {
                Console.WriteLine("{0} Customer Manager Added!!", _number);
            }
        }
        

        static class Teacher // static nesnelerde instance oluşturulmaz 
        {
            public static int Number { get; set; }
        }
    }
}
