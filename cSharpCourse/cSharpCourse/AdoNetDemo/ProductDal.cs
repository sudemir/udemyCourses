﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdoNetDemo
{
    internal class ProductDal
    {
        SqlConnection _connection = new SqlConnection(@"server=(localdb)\projectmodels;initial catalog=ETrade;integrated security=true");

        public List<Product> GetAll() 
        {
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }

            SqlCommand sqlCommand = new SqlCommand("select * from Products", _connection);

            SqlDataReader reader = sqlCommand.ExecuteReader();

            List<Product> products = new List<Product>();

            while (reader.Read())
            {
                Product product = new Product 
                {
                    Id = Convert.ToInt32(reader["Id"]),
                    Name = reader["Name"].ToString(),
                    StockAmount = Convert.ToInt32(reader["StockAmount"]),
                    UnitPrice = Convert.ToDecimal(reader["UnitPrice"])
                };
                products.Add(product);

            }
            reader.Close();
            _connection.Close();
            return products;   
            


        }


        public DataTable GetAll2()
        {
       
            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }

            SqlCommand sqlCommand = new SqlCommand("select * from Products", _connection);

            SqlDataReader reader = sqlCommand.ExecuteReader();

            DataTable dataTable = new DataTable();
            dataTable.Load(reader);
            reader.Close();
            _connection.Close();
            return dataTable;

        }

        public void Add(Product product)
        {

            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            SqlCommand command = new SqlCommand("INSERT INTO Products (Name,UnitPrice,StockAmount) values (@name, @unitPrice, @stockAmount)", _connection);

            command.Parameters.AddWithValue("@name", product.Name);
            command.Parameters.AddWithValue("@unitPrice", product.UnitPrice);
            command.Parameters.AddWithValue("@stockAmount", product.StockAmount);

            command.ExecuteNonQuery();
            _connection.Close() ;     
        }


        public void Update(Product product)
        {

            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            SqlCommand command = new SqlCommand("Update Products set Name=@name, UnitPrice=@unitPrice, StockAmount=@stockAmount where Id=@id", _connection);

            command.Parameters.AddWithValue("@name", product.Name);
            command.Parameters.AddWithValue("@unitPrice", product.UnitPrice);
            command.Parameters.AddWithValue("@stockAmount", product.StockAmount);
            command.Parameters.AddWithValue("@id", product.Id);


            command.ExecuteNonQuery();
            _connection.Close();
        }


        public void Delete(int id)
        {

            if (_connection.State == ConnectionState.Closed)
            {
                _connection.Open();
            }
            SqlCommand command = new SqlCommand("DELETE FROM Products WHERE Id=@id;", _connection);
            command.Parameters.AddWithValue("@id",id);


            command.ExecuteNonQuery();
            _connection.Close();
        }
    }
}
