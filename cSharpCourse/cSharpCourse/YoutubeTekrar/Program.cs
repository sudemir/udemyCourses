﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeTekrar
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Urun urun1 = new Urun();
            urun1.Adi = "Karpuz";
            urun1.Fiyati = 70;
            urun1.Aciklama = "Diyarbakır karpuzu";

            Urun urun2 = new Urun();
            urun2.Adi = "Çilek";
            urun2.Fiyati = 12;
            urun2.Aciklama = "Baldan tatlı çilekler";


            Urun[] urunler = new Urun[] {
                urun1, urun2
            };

            Console.WriteLine("Manavdaki ürürnler");

            foreach (Urun manavurunleri in urunler)
            {
                Console.WriteLine(manavurunleri.Adi + " , " + manavurunleri.Fiyati + " , " + manavurunleri.Aciklama);
            }

            Kurs kurs = new Kurs
            {
                KursAdi = "C#",
                Egitmen = "Engin",
                IzlenmeOrani = 12
            };
            Console.WriteLine(kurs.KursAdi);


            var kurs2 = kurs;
            kurs2.Egitmen = "Sümeyye";
            Console.WriteLine(kurs2.Egitmen);


            Console.WriteLine("Kurs Arrayinden sonraki aşama");

            Kurs[] kurslar = new Kurs[]
            {
                kurs, kurs2
            };

            foreach (var kursyazdır in kurslar)
            {
                Console.WriteLine(kursyazdır.Egitmen);
            }



            SepetManager sepetManager = new SepetManager();
            sepetManager.Ekle(urun1);
            sepetManager.Ekle(urun2);

            Console.ReadLine();
        }
    }

    class Kurs
    {
        public string KursAdi { get; set; }
        public string Egitmen { get; set; }
        public int IzlenmeOrani { get; set; }
    }
}
