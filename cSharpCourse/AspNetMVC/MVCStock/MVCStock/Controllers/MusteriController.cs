﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCStock.Models.Entity;
namespace MVCStock.Controllers
{
    public class MusteriController : Controller
    {
        // GET: Musteri
        MvcDbStockEntities db = new MvcDbStockEntities();
        public ActionResult Index()
        {
            var musteriler = db.TBLMUSTERİLER.ToList();
            return View(musteriler);
        }
        [HttpGet]
        public ActionResult YeniMusteri()
        {
            return View();
        }

        [HttpPost]
        public ActionResult YeniMusteri(TBLMUSTERİLER p1)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            db.TBLMUSTERİLER.Add(p1);
            db.SaveChanges();
            return View();
        }
        public ActionResult Sil(int id)
        {
            var musteri = db.TBLMUSTERİLER.Find(id);
            db.TBLMUSTERİLER.Remove(musteri);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult Guncelle(int id) 
        {
            var musteriGuncelle = db.TBLMUSTERİLER.Find(id);
            return View(musteriGuncelle);
        }
        public ActionResult MusteriGuncelle(TBLMUSTERİLER p1)
        {
            var musteriGetir = db.TBLMUSTERİLER.Find(p1.MusteriId);
            musteriGetir.MusteriAd = p1.MusteriAd;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}