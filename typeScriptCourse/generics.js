function deger(number) {
    return number;
}
console.log(deger(12));
function deger2(sehir) {
    return sehir;
}
console.log(deger2("Sümeyye"));
// Generics : değişkene hangi tipte değer atayacaksak o türde değer atamaya yardımcı olur
function deger3(deger) {
    return deger;
}
var sayi = deger3(90);
var deneme = deger3("Ankara");
console.log(sayi);
console.log(deneme);
// Generics Class
var GenericsClass = /** @class */ (function () {
    function GenericsClass() {
    }
    GenericsClass.prototype.kaydet = function (deneme) {
        return deneme;
    };
    return GenericsClass;
}());
var parametre = new GenericsClass();
var nam1 = parametre.degisken = "Sümeyye";
var nam2 = parametre.kaydet(12);
console.log(nam1);
console.log(nam2);
