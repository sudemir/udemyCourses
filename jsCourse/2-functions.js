var sayiler = Math.ceil(Math.random() * 49)
console.log("ilk kullanım = " + sayiler)


// ilk kullanım örneği
function denemeFonk() {
    return Math.ceil(Math.random() * 49)
}
console.log("ikinci kullanım = " + denemeFonk())


// ikinci kullanım örneği
function sayiuret() {
   console.log("üçüncu kullanım = " + Math.ceil(Math.random() * 49))
}
sayiuret()


// parametre aayarak kullanmak
function yenisayi(ustLimit) {
    return  Math.ceil(Math.random() * ustLimit)
}
console.log("dördüncü kullanım = " +yenisayi(10))


// varsayılan olarak üst limit atamak
function yenisayi2(maxDeger = 20) {
   return Math.ceil(Math.random() * maxDeger)
}
console.log("beşinci kullanım = " + yenisayi2())