﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EntityFramework
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        ProductDal _productDal = new ProductDal();
      
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadProduct();
        }

        private void LoadProduct()
        {
            dgwProducts.DataSource = _productDal.GetAll();

        }


        private void button1_Click(object sender, EventArgs e)
        {
            _productDal.Add(new Product
            {
                Name = textBox1.Text,
                UnitPrice = Convert.ToDecimal(textBox2.Text),
                StockAmount = Convert.ToInt32(textBox3.Text)
            });
            MessageBox.Show("Product Added!!");
            LoadProduct();

        }
        private void dgwProducts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
          
        }
        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void dgwProducts_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show("Hücre Seçildi");

            textBox4.Text = dgwProducts.CurrentRow.Cells[1].Value.ToString();
            textBox5.Text = dgwProducts.CurrentRow.Cells[2].Value.ToString();
            textBox6.Text = dgwProducts.CurrentRow.Cells[3].Value.ToString();

        }
    }
}
