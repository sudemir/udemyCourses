﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ActionResultSamples.Controllers
{
    public class EmptyController : Controller
    {
        // GET: Empty
      public void Index()
        {
            // EmptyResult bize boş bir sonuç döndürür. Bu kullanımı web sitelerindeki yetkilendirmeye göre belirli yerleri kullanıcıya gizleyip göstermek için kullanılabilir.
        }
        public ActionResult Index2()
        {
            return null;
        }
        public ActionResult Index3() // bu metodta ActionResult kullanmamızın sebebi aşağıda Contet Viewini kullanmmızdır
        {
            if(!User.Identity.IsAuthenticated)
            {
                return new EmptyResult(); // EmptyResult ve null ifadesini return etmek arasında hiçbir fark yoktur
            }
            return Content("Welcome Sudemir");
        }
    }
}