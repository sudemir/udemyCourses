﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PrivateProtectedInternalPublic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Customer customer = new Customer();
            customer.Add();
            
        }

        class Customer
        {
            private string name { get; set; } // private, sadece tanımlandığı class ya da method içinde kullanılabilir
            protected string isim { get; set; } // protected ise inherit edilen tüm classlarda kullanılabilir.
            public void Add() { 
                
            }
            public void Update()
            {

            }

        }
        class Student:Customer
        {
            
           
            public void Save() { 
            
            }

        }
    }
}
