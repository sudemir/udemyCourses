﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YoutubeTekrar
{
    internal class Urun
    {
        public int Id { get; set; }
        private string Name { get; set; }
        public string Adi
        {
            get
            {
                return Name + " ürünü";
            }
            set
            {
                Name = value;
            }
        }
        public int Fiyati { get; set; }

        public string Aciklama { get; set; }
    }


}
