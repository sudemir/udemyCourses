﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReacpDemo2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CustomerManager customerManager = new CustomerManager();
            customerManager.Add();
        }

        class CustomerManager
        {
            public void Add()
            {
                Logger logger = new Logger();
                logger.Log();
                Console.WriteLine("Added!!");
            }
        }

        class Logger
        {
            public void Log() {
                Console.WriteLine("Logged"); 
            }
        }
    }
}
