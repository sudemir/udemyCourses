﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions //C# üzerinde System.Exception sınıfından türeyen hata yapılarında kullanabileceğimiz çeşitli metot yapıları mevcuttur. Bu metotlar çoğu zaman projelerimizde nerede, nasıl hata oluştuğu konusunda bize fikir vererek yardımcı olan fonksiyonlardır.21 Ağu 2021

{
    internal class Program
    {
        static void Main(string[] args)
        {
			try
			{
				string[] sehirler = new string[3] { "ŞAnlıurfa", "Ankara", "Bursa" };
				sehirler[3] = "MArdin";
			}

			catch(DivideByZeroException ex) 
			{
				Console.WriteLine(ex.Message);
			}
			catch (Exception exception)
			{

				Console.WriteLine(exception.Message);
			}

			Console.ReadLine();	
        }
    }
}
