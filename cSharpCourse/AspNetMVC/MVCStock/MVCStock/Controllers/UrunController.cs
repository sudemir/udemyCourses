﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCStock.Models.Entity;

namespace MVCStock.Controllers
{
    public class UrunController : Controller
    {
        // GET: Urun

        MvcDbStockEntities db = new MvcDbStockEntities();
        public ActionResult Index()
        {
            var urunler = db.TBLURUNLER.ToList();
            return View(urunler);
        }
        [HttpGet]
        public ActionResult YeniUrun()
        {
            List<SelectListItem> degerler = (from i in db.TBLKATEGORILER.ToList() 
                                             select new SelectListItem
                                             {
                                                 Text = i.KategoryAd, // text ile kategri adını alıp değer olarak o kategori adına karşılık gelen kategori ıd yi almayı sağlıyoruz.
                                                 Value = i.KategoryId.ToString()
                                             }).ToList();
            ViewBag.dgr = degerler;
            return View();
        }
        [HttpPost]
        public ActionResult YeniUrun(TBLURUNLER p1)
        {

            var ktg = db.TBLKATEGORILER.Where(m => m.KategoryId == p1.TBLKATEGORILER.KategoryId).FirstOrDefault();
            p1.TBLKATEGORILER = ktg;
            db.TBLURUNLER.Add(p1);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Sil(int id)
        {
            var urun = db.TBLURUNLER.Find(id);
            db.TBLURUNLER.Remove(urun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Guncelle(int id)
        {
            var urunGuncelle = db.TBLURUNLER.Find(id);
            List<SelectListItem> degerler = (from i in db.TBLKATEGORILER.ToList()
                                             select new SelectListItem
                                             {
                                                 Text = i.KategoryAd, // text ile kategri adını alıp değer olarak o kategori adına karşılık gelen kategori ıd yi almayı sağlıyoruz.
                                                 Value = i.KategoryId.ToString()
                                             }).ToList();
            ViewBag.dgr = degerler;
            return View(urunGuncelle);
        }
        public ActionResult UrunGuncelle(TBLURUNLER p1)
        {
            var urunGuncel = db.TBLURUNLER.Find(p1.UrunId);
            urunGuncel.UrunAd = p1.UrunAd;
            urunGuncel.Marka= p1.Marka;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}